<?php

return [
    'server_ip' => env('EASYLINK_IP', '127.0.0.1'),

    'server_port' => env('EASYLINK_PORT', '8080'),
];