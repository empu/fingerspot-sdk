<?php

namespace Empu\FingerlinkSdk;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class FingerlinkSdkServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/fingerlink_sdk.php', 'fingerlink-sdk');

        $this->publishConfig();

        // $this->loadViewsFrom(__DIR__.'/resources/views', 'fingerlink-sdk');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->registerRoutes();
    }

    /**
     * Register the package routes.
     *
     * @return void
     */
    private function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/Http/routes.php');
        });
    }

    /**
    * Get route group configuration array.
    *
    * @return array
    */
    private function routeConfiguration()
    {
        return [
            'namespace'  => "Empu\FingerlinkSdk\Http\Controllers",
            'middleware' => 'api',
            'prefix'     => 'api'
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register facade
        $this->app->singleton('fingerlink-sdk', EasyLink::class);
    }

    /**
     * Publish Config
     *
     * @return void
     */
    public function publishConfig()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/fingerlink_sdk.php' => config_path('fingerlink_sdk.php'),
            ], 'config');
        }
    }
}
