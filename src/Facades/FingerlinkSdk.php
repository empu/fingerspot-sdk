<?php

namespace Empu\FingerlinkSdk\Facades;

use Illuminate\Support\Facades\Facade;

class FingerlinkSdk extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'fingerlink-sdk';
    }
}
