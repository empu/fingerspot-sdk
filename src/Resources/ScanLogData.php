<?php

namespace Empu\FingerlinkSdk\Resources;

use Carbon\Carbon;

class ScanLogData
{
    public $pin;
    public $workCode;
    public $sn;
    public $verifyMode;
    public $scanTime;
    public $ioMode;

    public function __construct(string $pin, int $workCode, string $sn, int $verifyMode, \DateTime $scanTime, int $ioMode)
    {
        $this->pin = $pin;
        $this->workCode = $workCode;
        $this->sn = $sn;
        $this->verifyMode = $verifyMode;
        $this->scanTime = $scanTime;
        $this->ioMode = $ioMode;
    }

    public static function fromRaw(array $data): self
    {
        $instance = new self(
            $data['PIN'],
            $data['WorkCode'],
            $data['SN'],
            $data['VerifyMode'],
            Carbon::createFromFormat('Y-m-d H:i:s', $data['ScanDate']),
            $data['IOMode']
        );

        return $instance;
    }
}