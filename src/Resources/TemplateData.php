<?php

namespace Empu\FingerlinkSdk\Resources;

use Empu\FingerlinkSdk\Resources\Struct;

class TemplateData extends Struct
{
    public function __construct (
        public string   $pin,
        public int   $idx,
        public string   $template,
        public string   $algVer,
    ) {}

    public function transform(): ?array
    {
        return [
            'pin' => $this->pin,
            'idx' => $this->idx,
            'template' => $this->template,
            'alg_ver' => $this->algVer,
        ];
    }
}
