<?php

namespace Empu\FingerlinkSdk\Resources;

use Empu\FingerlinkSdk\Exceptions\CannotMutateStructException;
use Illuminate\Support\Str;
use ReflectionClass;

abstract class Struct
{
    /**
     * @param string $name
     * @param mixed $value
     * @throws CannotMutateStructException
     */
    // public function __set(string $name, mixed $value): void
    // {
    //     throw new CannotMutateStructException(
    //         'Structs are immutable. If you need mutable data then use a class instead.'
    //     );
    // }

    public function all(): array
    {
        $reflector = new ReflectionClass(static::class);
        $response = [];

        foreach ($reflector->getProperties() as $property) {
            $response[$property->name] = $this->{$property->name};
        }

        return $response;
    }

    public function transform(): ?array
    {
        return null;
    }

    public function toArray(bool $snakeCase = false): array
    {
        $all = $this->transform() ?? self::all();

        if ($snakeCase === false) {
            return $all;
        }

        $snakeCaseAll = [];

        foreach ($all as $key => $value) {
            $snakeCaseAll[Str::snake($key)] =  $value;
        }

        return $snakeCaseAll;
    }
}