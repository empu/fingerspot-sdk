<?php

namespace Empu\FingerlinkSdk\Resources;

class UserData extends Struct
{
    public function __construct (
        public string   $pin,
        public string   $name,
        public string   $rfid,
        public string   $password,
        public int      $privilege,
        public array    $template,
    ) {}

    public static function makeFromJson(array $json): self
    {
        return new self(
            $json['PIN'],
            $json['Name'],
            $json['RFID'],
            $json['Password'],
            $json['Privilege'],
            $json['Template'],
        );
    }

    public function transform(): ?array
    {
        return [
            'PIN' => $this->pin,
            'Name' => $this->name,
            'RFID' => $this->rfid,
            'Password' => $this->password,
            'Privilege' => $this->privilege,
            'Template' => $this->template,
        ];
    }
}
