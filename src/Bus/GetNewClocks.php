<?php

namespace Empu\FingerlinkSdk\Bus;

use Empu\FingerlinkSdk\EasyLink;
use Empu\FingerlinkSdk\Resources\ScanLogData;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;

class GetNewClocks
{
    use Dispatchable;

    protected $host;
    protected $serialNumber;

    public function __construct(string $host, string $serialNumber)
    {
        $this->host = $host;
        $this->serialNumber = $serialNumber;
    }

    public function handle(EasyLink $connector): ?Collection
    {
        $response = $connector->connect($this->host, $this->serialNumber)
            ->fetchScanLogs();

        if ($response['Result'] === false) {
            return null;
        }

        return collect($response['Data'])
            ->map(function($value) {
                return ScanLogData::fromRaw($value);
            });
    }
}
