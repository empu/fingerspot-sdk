<?php

namespace Empu\FingerlinkSdk\Exceptions;

use LogicException;

class CannotMutateStructException extends LogicException {}
