<?php

namespace Empu\FingerlinkSdk;

use Empu\FingerlinkSdk\Resources\ScanLogData;
use Empu\FingerlinkSdk\Resources\UserData;
use Error;
use GuzzleHttp\Client;

class EasyLink
{
    protected $host;

    protected $serialNumber;

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function connect(string $host, string $serialNumber): self
    {
        $this->host = $host;
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function initDevice(): array
    {
        return $this->sendRequest('/dev/init');
    }

    public function getDeviceInfo(): array
    {
        return $this->sendRequest('/dev/info');
    }

    public function setDeviceTime(): array
    {
        return $this->sendRequest('/dev/settime');
    }

    public function truncateDeviceLogs(): array
    {
        return $this->sendRequest('/log/del');
    }

    public function fetchUsers(): array
    {
        return $this->sendRequest('/user/all/paging');
    }

    public function uploadUser(UserData $user): array
    {
        return $this->sendRequest('/user/set', $user->toArray());
    }

    public function deleteUser(UserData|string $user): array
    {
        $pin = ($user instanceof UserData) ? $user->PIN : $user;

        return $this->sendRequest('/user/del', ['pin' => $pin]);
    }

    public function truncateUsers(): array
    {
        return $this->sendRequest('/user/delall');
    }

    public function fetchScanLogs(int $limit = 10): array
    {
        return $this->sendRequest('/scanlog/all/paging');
    }

    public function getNewScanLogs(): array
    {
        $result = $this->sendRequest('/scanlog/new');
        $data = $result['Data'] ?? [];

        return array_map(fn($item): ScanLogData => ScanLogData::fromRaw($item), $data);
    }

    public function truncateScanLogs(): array
    {
        return $this->sendRequest('/scanlog/del');
    }

    public function sendRequest(string $path, array $params = []): ?array
    {
        $params = array_merge(['sn' => $this->serialNumber], $params);
        $uri = trim($this->host, '/') . rtrim($path, '/');
        $response = $this->client->request('POST', $uri, [
            'query' => $this->sanitizeParameters($params)
        ]);
        $content = $response->getBody();
        $result = json_decode($content, true);

        return $result;
    }

    protected function sanitizeParameters(array $params): array
    {
        foreach ($params as &$value) {
            if (is_array($value) || $value instanceof \stdClass) {
                $value = json_encode($value);
            } else {
                $value = strval($value);
            }
        }

        return $params;
    }
}

