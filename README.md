# fingerlink-sdk

[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Travis](https://img.shields.io/travis/empu/fingerlink-sdk.svg?style=flat-square)]()
[![Total Downloads](https://img.shields.io/packagist/dt/empu/fingerlink-sdk.svg?style=flat-square)](https://packagist.org/packages/empu/fingerlink-sdk)


## Install

```bash
composer require empu/fingerlink-sdk
```


## Usage

Write a few lines about the usage of this package.


## Testing

Run the tests with:

```bash
vendor/bin/phpunit
```


## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.


## Security

If you discover any security-related issues, please email wuri.nugrahadi@gmail.com instead of using the issue tracker.


## License

The MIT License (MIT). Please see [License File](/LICENSE.md) for more information.