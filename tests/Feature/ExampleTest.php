<?php

namespace Empu\FingerlinkSdk\Tests\Feature;

use Empu\FingerlinkSdk\Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
